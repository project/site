<?php

namespace Drupal\site\Entity\ProjectBundle;

use Drupal\site\Entity\Project;

/**
 * A base bundle class for project entities.
 */
abstract class ProjectBundle extends Project {

}
