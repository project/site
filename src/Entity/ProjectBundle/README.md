# Project Bundles

Project Bundle classes are used to differentiate between things.

They inherit each other so different kinds of project types can be created with fields from other bundles.

1. ProjectBundle. Base Class.
2. WebProject. Canonical Url field.
3. CodeProject. Git Remote field
4. DrupalProject. Drupal Site UUID.

