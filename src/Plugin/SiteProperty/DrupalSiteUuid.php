<?php

namespace Drupal\site\Plugin\SiteProperty;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\site\Entity\SiteDefinition;
use Drupal\site\Entity\SiteEntity;
use Drupal\site\SitePropertyPluginBase;

/**
 * Plugin implementation of the site_property.
 *
 * @SiteProperty(
 *   id = "drupal_site_uuid",
 *   name = "drupal_site_uuid",
 *   site_bundles = {
 *     "Drupal\site\Entity\Bundle\DrupalSiteBundle",
 *   },
 *   label = @Translation("Drupal Site UUID"),
 *   description = @Translation("The unique identifyer of this Drupal site.")
 * )
 */
class DrupalSiteUuid extends SitePropertyPluginBase {

  public function value() {
    return \Drupal::config('system.site')->get('uuid');
  }

  /**
   * {@inheritdoc }
   */
  static public function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields['drupal_site_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Drupal Site UUID'))
      ->setRevisionable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
    ;

    return $fields;
  }

}
