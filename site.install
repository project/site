<?php

/**
 * @file
 * Install, update and uninstall functions for the Site module.
 */

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Utility\UpdateException;
use Drupal\site\Entity\Project;

/**
 * Implements hook_install().
 */
function site_install() {

  // Ok this is crazy.
  // This should not be necessary, but...
  // See site_entity_field_storage_info()
  // See site_entity_field_storage_info call allPropertyFieldDefinitions
  // See allPropertyFieldDefinitions return ALL fields from ALL bundles.
  // See allPropertyFieldDefinitions return NO FIELDS when installing site.module.
  // See empty $bundles = $EntityTypeBundleInfo->getBundleInfo($entity_type->id());
  // Every time.
  // Had to use debugging and install via the extend form to figure this out. :D

  // So this hack just reinstalls all the fields.
  // This is the fastest way I could figure out how to do this.
  // Stolen from DevelEntityDefinitionUpdateManager

  // reinstall storage
  $changes = \Drupal::entityDefinitionUpdateManager()
    ->getChangeList();

  // @TODO: Move to something.
  foreach ($changes as $entity_type_id => $change_list) {
    // Process entity type definition changes before storage definitions ones
    // this is necessary when you change an entity type from non-revisionable
    // to revisionable and at the same time add revisionable fields to the
    // entity type.
    if (!empty($change_list['entity_type'])) {
      //$this->doEntityUpdate($change_list['entity_type'], $entity_type_id);
      \Drupal::entityDefinitionUpdateManager()
        ->updateEntityType($change_list['entity_type']);
    }

    // Process field storage definition changes.
    if (!empty($change_list['field_storage_definitions'])) {

      $storage_definitions = \Drupal::service('entity_field.manager')->getFieldStorageDefinitions($entity_type_id);
      $original_storage_definitions = \Drupal::service('entity.last_installed_schema.repository')->getLastInstalledFieldStorageDefinitions($entity_type_id);

      foreach ($change_list['field_storage_definitions'] as $field_name => $change) {
        $storage_definition = isset($storage_definitions[$field_name]) ? $storage_definitions[$field_name] : NULL;
        $original_storage_definition = isset($original_storage_definitions[$field_name]) ? $original_storage_definitions[$field_name] : NULL;
        //$this->doFieldUpdate($change, $storage_definition, $original_storage_definition);

        $op = $change;
        switch ($op) {
          case EntityDefinitionUpdateManagerInterface::DEFINITION_CREATED:
            \Drupal::service('field_storage_definition.listener')->onFieldStorageDefinitionCreate($storage_definition);
            break;

          case EntityDefinitionUpdateManagerInterface::DEFINITION_UPDATED:
            if ($storage_definition && $original_storage_definition) {
              \Drupal::service('field_storage_definition.listener')->onFieldStorageDefinitionUpdate($storage_definition, $original_storage_definition);
            }
            break;

          case EntityDefinitionUpdateManagerInterface::DEFINITION_DELETED:
            if ($original_storage_definition) {
              \Drupal::service('field_storage_definition.listener')->onFieldStorageDefinitionDelete($original_storage_definition);
            }
            break;
        }

      }
    }
  }

  // @TODO: I would create a site entity here... but when enabled during an install profile,
  // the module is enabled BEFORE site title and URL are available... so.... just clear caches for first entity??
}

/**
 * Implements hook_uninstall().
 */
function site_uninstall() {
}

/**
 * Install site_root composer_json and drupal_env fields.
 */
function site_update_8001() {
  $fields['drupal_env'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Drupal Environment'))
    ->setDescription(t('The value of the DRUPAL_ENV environment variable.'))
    ->setRevisionable(TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
    ])
  ;
  $fields['site_root'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Root path'))
    ->setDescription(t('The root path of this site.'))
    ->setRevisionable(TRUE)
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
    ])
  ;
  $fields['composer_json'] = BaseFieldDefinition::create('map')
    ->setLabel(t('Composer.json Data'))
    ->setRevisionable(TRUE)
    ->setDisplayConfigurable('view', TRUE)
  ;
  foreach ($fields as $name => $field) {
    Drupal::entityDefinitionUpdateManager()
      ->installFieldStorageDefinition($name, 'site', 'site', $field);
  }
}

/**
 * Install api_key field on Drupal Project entity.
 */
function site_update_8002() {
  $fields['api_key'] = BaseFieldDefinition::create('string')
    ->setLabel(t('API Key'))
    ->setDescription(t('An API Key from the site. If entered, site data from here will post back to the client site when saving this form.'))
    ->setRevisionable(TRUE)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
    ])
    ->setDisplayConfigurable('form', TRUE)
  ;
  foreach ($fields as $name => $field) {
    Drupal::entityDefinitionUpdateManager()
      ->installFieldStorageDefinition($name, 'drupal_project', 'drupal_project', $field);
  }
}


/**
 * Remove composer_json field. Plugin stores in data field now.
 *
 * For some reason, this field was breaking basic website creation.
 */
function site_update_8003() {
  $definition_manager = \Drupal::entityDefinitionUpdateManager();
  $field_storage_definition = $definition_manager->getFieldStorageDefinition('composer_json', 'site');
  $definition_manager->uninstallFieldStorageDefinition($field_storage_definition);
}

/**
 * Install project entity, Drupal Project bundle, and fields.
 */
function site_update_8004()
{
  try {
    $definition = \Drupal::service('entity_type.manager')->getDefinition('project');
    \Drupal::entityDefinitionUpdateManager()->installEntityType($definition);

    $definition = \Drupal::service('entity_type.manager')->getDefinition('project_type');
    \Drupal::entityDefinitionUpdateManager()->installEntityType($definition);

  } catch (\Exception $exception) {
    throw new UpdateException($exception->getMessage());
  }

  $fields['label'] = BaseFieldDefinition::create('string')
    ->setRevisionable(TRUE)
    ->setTranslatable(TRUE)
    ->setLabel(t('Label'))
    ->setRequired(TRUE)
    ->setSetting('max_length', 255)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -5,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -5,
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['status'] = BaseFieldDefinition::create('boolean')
    ->setRevisionable(TRUE)
    ->setLabel(t('Status'))
    ->setDefaultValue(TRUE)
    ->setSetting('on_label', 'Enabled')
    ->setDisplayOptions('form', [
      'type' => 'boolean_checkbox',
      'settings' => [
        'display_label' => FALSE,
      ],
      'weight' => 0,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('view', [
      'type' => 'boolean',
      'label' => 'above',
      'weight' => 0,
      'settings' => [
        'format' => 'enabled-disabled',
      ],
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['description'] = BaseFieldDefinition::create('text_long')
    ->setRevisionable(TRUE)
    ->setTranslatable(TRUE)
    ->setLabel(t('Description'))
    ->setDisplayOptions('form', [
      'type' => 'text_textarea',
      'weight' => 10,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('view', [
      'type' => 'text_default',
      'label' => 'above',
      'weight' => 10,
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['uid'] = BaseFieldDefinition::create('entity_reference')
    ->setRevisionable(TRUE)
    ->setTranslatable(TRUE)
    ->setLabel(t('Author'))
    ->setSetting('target_type', 'user')
    ->setDefaultValueCallback('Drupal/site/Entity/Project::getDefaultEntityOwner')
    ->setDisplayOptions('form', [
      'type' => 'entity_reference_autocomplete',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'size' => 60,
        'placeholder' => '',
      ],
      'weight' => 15,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'author',
      'weight' => 15,
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['created'] = BaseFieldDefinition::create('created')
    ->setLabel(t('Authored on'))
    ->setTranslatable(TRUE)
    ->setDescription(t('The time that the project was created.'))
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'timestamp',
      'weight' => 20,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'datetime_timestamp',
      'weight' => 20,
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['changed'] = BaseFieldDefinition::create('changed')
    ->setLabel(t('Changed'))
    ->setTranslatable(TRUE)
    ->setDescription(t('The time that the project was last edited.'));

  $fields['drupal_site_uuid'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Site UUID'))
    ->setDescription(t('The Drupal site UUID.'))
    ->setRequired(false)
    ->setReadOnly(true)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'string',
    ])
    ->setDisplayConfigurable('view', TRUE);

  $fields['canonical_url'] = BaseFieldDefinition::create('uri')
    ->setRevisionable(TRUE)
    ->setRequired(FALSE)
    ->setLabel(t('Canonical URL'))
    ->setDescription(t('The primary live URL for this site.'))
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -10,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'inline',
      'type' => 'link',
    ])
  ;
  $fields['git_remote'] = BaseFieldDefinition::create('string')
    ->setLabel(t('Git Remote'))
    ->setDescription(t('The clone URL for the git repository that stores this sites code.'))
    ->setRevisionable(TRUE)
    ->setRequired(FALSE)
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'string',
    ])
  ;
  foreach ($fields as $name => $field) {
    Drupal::entityDefinitionUpdateManager()
      ->installFieldStorageDefinition($name, 'project', 'project', $field);
  }

  $project_type = \Drupal\site\Entity\ProjectType::create([
    'id' => 'drupal',
    'label' => 'Drupal Project',
  ]);
  $project_type->save();

  // Project field.
  Drupal::entityDefinitionUpdateManager()
    ->installFieldStorageDefinition('project', 'site', 'site', BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Project'))
      ->setDescription(t('The project that this site belongs to.'))
      ->setRevisionable(TRUE)
      ->addConstraint('ProjectExistsConstraint')
      ->setSetting('target_type', 'project')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
    );

  // Reinstall config from site.module.
  \Drupal::service('config.installer')->installDefaultConfig('module', 'site');
}

/**
 * Save a Project entity for each Drupal Project
 */
function site_update_8005() {

  $projects = \Drupal::entityTypeManager()->getStorage('drupal_project')
    ->loadByProperties();
  foreach ($projects as $drupal_project) {
    $values = $drupal_project->toArray();
    $values['project_type'] = 'drupal';
    $values['label'] = $drupal_project->label();
    $values['created'] = time();
    $values['changed'] = time();

    unset($values['revision_id']);
    unset($values['uuid']);
    $project_entity = Project::create($values);
    $project_entity->save();

    \Drupal::messenger()->addStatus(t("New Project (:project) entity created from Drupal Project Entity (:drupal_project)", [
      ':project' => $project_entity->toUrl()->setOption('absolute', true)->toString(),
      ':drupal_project' => $drupal_project->toUrl()->setOption('absolute', true)->toString(),
    ]));

    $sites = \Drupal::entityTypeManager()->getStorage('site')
      ->loadByProperties(['drupal_project' => $drupal_project->id()]);
    foreach ($sites as $site) {
      $site->set('project', $project_entity->id());
      $site->save();
    }
  }

  \Drupal::messenger()->addStatus(t("New Project entities have been created. You should delete the old ones once you are sure you do not need the data."));

}

/**
 * Delete all Drupal Project entities, remove the entity definition, and remove the drupal_project field from sites.
 */
function site_update_8007() {

  $projects = \Drupal::entityTypeManager()->getStorage('drupal_project')
    ->loadByProperties();
  foreach ($projects as $drupal_project) {
    $drupal_project->delete();
  }

  try {
    // Uninstall drupal_project field.
    $definition_manager = \Drupal::entityDefinitionUpdateManager();
    $field_storage_definition = $definition_manager->getFieldStorageDefinition('drupal_project', 'site');
    if ($field_storage_definition) {
      $definition_manager->uninstallFieldStorageDefinition($field_storage_definition);
    }

    // Uninstall drupal_project entity.
    $definition = \Drupal::service('entity_type.manager')->getDefinition('drupal_project');
    \Drupal::entityDefinitionUpdateManager()->uninstallEntityType($definition);
  } catch (\Exception $exception) {
    throw new UpdateException($exception->getMessage());
  }
}
